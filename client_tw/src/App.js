import React, { useEffect, useState } from "react";
import Navbar from "./components/Navbar";
import Main from "./components/Main";
import playersService from "./services/players";

function App() {
  const [players, setPlayers] = useState([]);

  useEffect(() => {
    playersService
      .getAllPlayers()
      .then((response) => setPlayers(response))
      .catch((e) => console.error(e.message));
  }, []);

  if (players.length === 0) {
    setPlayers([
      {
        username: "herlambang",
        password:
          "$2b$10$XkOwsa3Ok3GbbvsSEJ..eeMMN5SaSCXcdYhUfg8/A7pW2ikN3V7yy",
        email: "herlambangk25@mail.com",
        experience: 150,
        level: 10,
      },
      {
        username: "dada",
        password: "98597712-bd1c-4942-a887-fc0ca51b6d5f",
        email: "dada@gmail.com",
        experience: 90,
        level: 7,
      },
      {
        username: "hehe",
        password: "2374e89b-a80b-40e0-9dd3-4e58f246375d",
        email: "hehe@gmail.com",
        experience: 170,
        level: 11,
      },
    ]);
  }
  const classes = {
    wrapper:
      "overflow-x-hidden lg:h-screen lg:overflow-hidden grid grid-cols-1 sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-6 xl:grid-cols-6",
    navClass: "bg-gray-800 col-span-1 flex flex-col items-start",
    mainClass: "overflow-y-auto sm:h-full md:h-full bg-gray-300 col-span-5 p-3",
  };

  return (
    <div className={classes.wrapper}>
      <Navbar navClass={classes.navClass} />
      <Main allPlayers={players} mainClass={classes.mainClass} />
    </div>
  );
}

export default App;
