// // setup database here, change the values to suit your environment
// module.exports = {
//   HOST: "localhost",
//   USER: "postgres",
//   PASSWORD: "123456",
//   DB: "binarchallenge7",
//   dialect: "postgres",
//   pool: {
//     max: 5,
//     min: 0,
//     acquire: 30000,
//     idle: 10000,
//   },
// };

require("dotenv").config();

module.exports = {
  development: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
    // port: process.env.DB_PORT,
  },
};
